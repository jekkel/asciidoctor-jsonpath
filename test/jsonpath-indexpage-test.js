/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
require('@asciidoctor/core')()

const { expect } = require('chai')
const EventEmitter = require('events')
const jsonpath = require('./../lib/jsonpath')
const classifyContent = require('@antora/content-classifier')

const primarySiteUrl = 'https://example.com'
const path = __dirname

describe('jsonpath indexpage tests', () => {
  let eventEmitter
  let log

  beforeEach(() => {
    eventEmitter = new EventEmitter()
    log = []
    jsonpath.prependListener = (event, code) => {
      eventEmitter.prependListener(event, code)
    }
    jsonpath.on = (event, code) => {
      eventEmitter.on(event, code)
    }
    jsonpath.getLogger = (name) => {
      return {
        trace: (msg) => {
          log.push({ level: 'trace', msg })
        },
        debug: (msg) => {
          log.push({ level: 'debug', msg })
        },
        info: (msg) => {
          log.push({ level: 'info', msg })
        },
        warn: (msg) => {
          log.push({ level: 'warn', msg })
        },
        isLevelEnabled: (level) => true,
      }
    }
  })

  it('no indexpages test', () => {
    jsonpath.register(jsonpath, { config: { logLevel: 'debug' }, playbook: {} })
    expect(eventEmitter.eventNames().length).to.equal(1)
    eventEmitter.eventNames().forEach((name) => {
      expect(eventEmitter.listenerCount(name)).to.equal(1)
    })
  })
  it('indexpages test', () => {
    jsonpath.register(jsonpath, { config: { logLevel: 'debug', indexPages: [] }, playbook: {} })
    expect(eventEmitter.eventNames().length).to.equal(2)
    const counts = { contentAggregated: 1, contentClassified: 2 }
    eventEmitter.eventNames().forEach((name) => {
      expect(eventEmitter.listenerCount(name)).to.equal(counts[name])
    })
  })

  describe('tests with content', () => {
    let contentCatalog

    beforeEach(() => {
      contentCatalog = classifyContent({ site: {} }, [], {})
      const baseFile = {
        asciidoc: { xreftext: 'doctitle' },
        origin: { site: primarySiteUrl },
        out: undefined,
        pub: {
          url: primarySiteUrl + '/c1/1.0/index.html',
          moduleRootPath: '',
        },
        src: {
          component: 'c1',
          version: '1.0',
          module: 'ROOT',
          relative: 'index.adoc',
          family: 'page',
        },
        path,
      }
      const file = contentCatalog.addFile(baseFile)
      contentCatalog.addFile({
        src: Object.assign({}, file.src, { family: 'example', relative: 'templates/template1.adoc' }),
        // eslint-disable-next-line no-template-curly-in-string
        contents: Buffer.from('= This is the template for ${path[2]}\n\nkind: ${value.kind}\n\njava type: ${value.javaType}\n\ndescription: ${value.description}'),
      })

      contentCatalog.addFile({
        src: Object.assign({}, file.src, { family: 'example', relative: 'json/data2.json' }),
        // eslint-disable-next-line no-template-curly-in-string
        contents: Buffer.from(`{
  "component": {
    "kind": "component",
    "name": "activemq",
    "title": "ActiveMQ",
    "description": "Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.",
    "deprecated": false,
    "firstVersion": "1.0.0",
    "label": "messaging",
    "javaType": "org.apache.camel.component.activemq.ActiveMQComponent",
    "supportLevel": "Stable",
    "groupId": "org.apache.camel",
    "artifactId": "camel-activemq",
    "version": "3.5.0-SNAPSHOT",
    "scheme": "activemq",
    "extendsScheme": "jms",
    "syntax": "activemq:destinationType:destinationName",
    "async": true,
    "consumerOnly": false,
    "producerOnly": false,
    "lenientProperties": false
  },
  "componentProperties": {
    "brokerURL": { "kind": "property", "displayName": "Broker URL", "group": "common", "label": "common", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "deprecationNote": "", "secret": false, "description": "Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)" },
    "clientId": { "kind": "property", "displayName": "Client Id", "group": "common", "label": "", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead." },
    "connectionFactory": { "kind": "property", "displayName": "Connection Factory", "group": "common", "label": "", "required": false, "type": "object", "javaType": "javax.jms.ConnectionFactory", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "The connection factory to be use. A connection factory must be configured either on the component or endpoint." },
    "disableReplyTo": { "kind": "property", "displayName": "Disable Reply To", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "secret": false, "defaultValue": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Specifies whether Camel ignores the JMSReplyTo header in messages. If true, Camel does not send a reply back to the destination specified in the JMSReplyTo header. You can use this option if you want Camel to consume from a route and you do not want Camel to automatically send back a reply message because another component in your code handles the reply message. You can also use this option if you want to use Camel as a proxy between different message brokers and you want to route message from one system to another." },
    "durableSubscriptionName": { "kind": "property", "displayName": "Durable Subscription Name", "group": "common", "label": "", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "The durable subscriber name for specifying durable topic subscriptions. The clientId option must be configured as well." },
    "jmsMessageType": { "kind": "property", "displayName": "Jms Message Type", "group": "common", "label": "", "required": false, "type": "object", "javaType": "org.apache.camel.component.jms.JmsMessageType", "enum": [ "Bytes", "Map", "Object", "Stream", "Text" ], "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Allows you to force the use of a specific javax.jms.Message implementation for sending JMS messages. Possible values are: Bytes, Map, Object, Stream, Text. By default, Camel would determine which JMS message type to use from the In body type. This option allows you to specify it." }
  },
  "properties": {
    "httpUri": { "kind": "path", "displayName": "Http Uri", "group": "common", "label": "", "required": true, "type": "string", "javaType": "java.net.URI", "deprecated": false, "deprecationNote": "", "autowired": false, "secret": false, "description": "The URI to use such as http:\\/\\/hostname:port\\/path" },
    "bridgeEndpoint": { "kind": "parameter", "displayName": "Bridge Endpoint", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "autowired": false, "secret": false, "defaultValue": false, "description": "If the option is true, then the Exchange.HTTP_URI header is ignored, and use the endpoint's URI for request. You may also set the throwExceptionOnFailure to be false to let the AhcProducer send all the fault response back." },
    "bufferSize": { "kind": "parameter", "displayName": "Buffer Size", "group": "common", "label": "", "required": false, "type": "integer", "javaType": "int", "deprecated": false, "autowired": false, "secret": false, "defaultValue": 4096, "description": "The initial in-memory buffer size used when transferring data between Camel and AHC Client." },
    "headerFilterStrategy": { "kind": "parameter", "displayName": "Header Filter Strategy", "group": "common", "label": "", "required": false, "type": "object", "javaType": "org.apache.camel.spi.HeaderFilterStrategy", "deprecated": false, "autowired": false, "secret": false, "description": "To use a custom HeaderFilterStrategy to filter header to and from Camel message." },
    "throwExceptionOnFailure": { "kind": "parameter", "displayName": "Throw Exception On Failure", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "autowired": false, "secret": false, "defaultValue": true, "description": "Option to disable throwing the AhcOperationFailedException in case of failed responses from the remote server. This allows you to get all responses regardless of the HTTP status code." }
  },
  "apis": {
    "account": { "consumerOnly": false, "producerOnly": false, "description": "", "aliases": [ "^creator$=create", "^deleter$=delete", "^fetcher$=fetch", "^reader$=read", "^updater$=update" ], "methods": { "fetcher": { "description": "Create a AccountFetcher to execute fetch", "signatures": [ "com.twilio.rest.api.v2010.AccountFetcher fetcher()", "com.twilio.rest.api.v2010.AccountFetcher fetcher(String pathSid)" ] }, "updater": { "description": "Create a AccountUpdater to execute update", "signatures": [ "com.twilio.rest.api.v2010.AccountUpdater updater()", "com.twilio.rest.api.v2010.AccountUpdater updater(String pathSid)" ] } } },
    "address": { "consumerOnly": false, "producerOnly": false, "description": "", "aliases": [ "^creator$=create", "^deleter$=delete", "^fetcher$=fetch", "^reader$=read", "^updater$=update" ], "methods": { "creator": { "description": "Create a AddressCreator to execute create", "signatures": [ "com.twilio.rest.api.v2010.account.AddressCreator creator(String customerName, String street, String city, String region, String postalCode, String isoCountry)", "com.twilio.rest.api.v2010.account.AddressCreator creator(String pathAccountSid, String customerName, String street, String city, String region, String postalCode, String isoCountry)" ] }, "deleter": { "description": "Create a AddressDeleter to execute delete", "signatures": [ "com.twilio.rest.api.v2010.account.AddressDeleter deleter(String pathAccountSid, String pathSid)", "com.twilio.rest.api.v2010.account.AddressDeleter deleter(String pathSid)" ] }, "fetcher": { "description": "Create a AddressFetcher to execute fetch", "signatures": [ "com.twilio.rest.api.v2010.account.AddressFetcher fetcher(String pathAccountSid, String pathSid)", "com.twilio.rest.api.v2010.account.AddressFetcher fetcher(String pathSid)" ] }, "reader": { "description": "Create a AddressReader to execute read", "signatures": [ "com.twilio.rest.api.v2010.account.AddressReader reader()", "com.twilio.rest.api.v2010.account.AddressReader reader(String pathAccountSid)" ] }, "updater": { "description": "Create a AddressUpdater to execute update", "signatures": [ "com.twilio.rest.api.v2010.account.AddressUpdater updater(String pathAccountSid, String pathSid)", "com.twilio.rest.api.v2010.account.AddressUpdater updater(String pathSid)" ] } } },
  }
}
`),
      })
    })

    // afterEach(() => {
    //   if (log.length) console.log('log', log)
    // })

    const config1 = {
      playbook: {},
      config: {
        log_level: 'debug',
        indexPages: [
          {
            query: {
              version: '1.0',
              component: 'c1',
              module: 'ROOT',
              // family: 'page',
              target: 'example$json/data2.json',
              query: 'nodes$.componentProperties.*',
            },
            templateId: {
              family: 'example',
              relative: 'templates/template1.adoc',
            },
            target: {
              // eslint-disable-next-line no-template-curly-in-string
              format: '`generated/${path[2]}.adoc`',
            },
          },
        ],
      },
    }

    it('expected page count', () => {
      jsonpath.register(jsonpath, config1)
      expect(eventEmitter.eventNames().length).to.equal(2)
      eventEmitter.emit('contentClassified', { contentCatalog, siteAsciiDocConfig: {} })
      expect(contentCatalog.getFiles().length).to.equal(3 + 6)
      expect(log.pop().msg.msg).to.equal('added 6 pages')
    })

    it('basic content', () => {
      jsonpath.register(jsonpath, config1)
      expect(eventEmitter.eventNames().length).to.equal(2)
      eventEmitter.emit('contentClassified', { contentCatalog, siteAsciiDocConfig: {} })
      expect(contentCatalog.getFiles().length).to.equal(3 + 6)
      expect(log.pop().msg.msg).to.equal('added 6 pages')
      const content = contentCatalog.resolvePage('1.0@c1:ROOT:generated/jmsMessageType.adoc').contents.toString()
      expect(content).to.equal(`= This is the template for jmsMessageType

kind: property

java type: org.apache.camel.component.jms.JmsMessageType

description: Allows you to force the use of a specific javax.jms.Message implementation for sending JMS messages. Possible values are: Bytes, Map, Object, Stream, Text. By default, Camel would determine which JMS message type to use from the In body type. This option allows you to specify it.`)
    })
  })
})
