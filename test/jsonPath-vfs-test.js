/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const jsonpath = require('./../lib/jsonpath')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

const delimiter = '`'

describe('jsonpath tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
    const loggerManager = asciidoctor.LoggerManager
    const defaultLogger = loggerManager.getLogger()
    const jsonFormatter = asciidoctor.LoggerManager.newFormatter('JsonFormatter', {
      call: function (severity, time, programName, message) {
        // const text = message['text']
        // const sourceLocation = message['source_location']
        return JSON.stringify({
          programName: programName,
          time,
          message,
          severity,
        }) + '\n'
      },
    })
    defaultLogger.setFormatter(jsonFormatter)
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            read: (filespec) => {
              filespec = filespec.substring(filespec.lastIndexOf(':') + 1).substring(filespec.lastIndexOf('$') + 1)
              return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
                accum.contents = Buffer.from(f.contents)
                return accum
              }, {}).contents
            },

          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: {
            src: {
              version: '4.5',
              component: 'component-a',
              module: 'module-a',
              family: 'page',
            },
          },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
    ]
    return vfss
  }

  ;[{
    type: 'global',
    f: (text, config) => {
      jsonpath.register(asciidoctor.Extensions, config)
      return asciidoctor.load(text)
    },
  },
  {
    type: 'registry',
    f: (text, config) => {
      const registry = jsonpath.register(asciidoctor.Extensions.create(), config)
      return asciidoctor.load(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    prepareVfss([
      {
        version: '4.5',
        family: 'page',
        relative: 'page-a.adoc',
        contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
      },
      {
        version: '4.5',
        family: 'example',
        relative: 'data1.json',
        contents: `{
"rows": [
  {
    "name": "foo",
    "description": "fooish",
    "uniqueish": 1
  },
  {
    "name": "bar",
    "description": "barish",
    "uniqueish": 2
  }
  ],
  "object": {
    "field1": {
      "p1": "a",
      "p2": "b",
      "uniqueish": 1
    },
    "field2": {
      "p1": "c",
      "p2": "d",
      "uniqueish": 1
    },
    "field3": {
      "p1": "e",
      "p2": "f",
      "uniqueish": 2
    }
  }
}`,
      },
      {
        version: '4.5',
        family: 'example',
        relative: 'data1-json5.json',
        contents: `{
rows: [
  {
    name: 'foo',
    'description': "fooish",
    uniqueish: 1
  },
  {
    name: "bar",
    description: "barish",
    uniqueish: 2
  },
  ],
  object: {
    field1: {
      p1: "a",
      p2: "b",
      uniqueish: 1
    },
    field2: {
      p1: "c",
      p2: "d",
      uniqueish: 1
    },
    field3: {
      p1: "e",
      p2: "f",
      uniqueish: 2
    },
  },
}`,
      },
      {
        version: '4.5',
        family: 'example',
        relative: 'data1.toml',
        contents: `[[rows]]
  name = "foo"
  description = "fooish"
  uniqueish = 1
[[rows]]
  name = "bar"
  description = "barish"
  uniqueish = 2 
[object] 
  [object.field1]
    p1 = "a"
    p2 = "b"
    uniqueish = 1
  [object.field2]
    p1 = "c"
    p2 = "d"
    uniqueish = 1
  [object.field3]
    p1 = "e"
    p2 = "f"
    uniqueish = 2
`,
      },
      {
        version: '4.5',
        family: 'example',
        relative: 'data1.yml',
        contents: `rows:
  - name: foo
    description: fooish
    uniqueish: 1
  - name: bar
    description: barish
    uniqueish: 2
object:
  field1:
    p1: a
    p2: b
    uniqueish: 1
  field2:
    p1: c
    p2: d
    uniqueish: 1
  field3:
    p1: e
    p2: f
    uniqueish: 2
`,
      },
      //Content presumably AL2 licensed, from Apache Camel.
      {
        version: '4.5',
        family: 'example',
        relative: 'data2.json',
        contents: `{
  "component": {
    "kind": "component",
    "name": "activemq",
    "title": "ActiveMQ",
    "description": "Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.",
    "multilinedescription": "Send messages to (or consume from) Apache ActiveMQ.\\n\\nThis component extends the Camel JMS component.",
    "specialChars": "Here are some <&> special < characters & with pointy > brackets>><<",
    "deprecated": false,
    "firstVersion": "1.0.0",
    "label": "messaging",
    "javaType": "org.apache.camel.component.activemq.ActiveMQComponent",
    "supportLevel": "Stable",
    "groupId": "org.apache.camel",
    "artifactId": "camel-activemq",
    "version": "3.5.0-SNAPSHOT",
    "scheme": "activemq",
    "extendsScheme": "jms",
    "syntax": "activemq:destinationType:destinationName",
    "async": true,
    "consumerOnly": false,
    "producerOnly": false,
    "lenientProperties": false
  },
  "componentProperties": {
    "brokerURL": { "kind": "property", "displayName": "Broker URL", "group": "common", "label": "common", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "deprecationNote": "", "secret": false, "description": "Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)" },
    "clientId": { "kind": "property", "displayName": "Client Id", "group": "common", "label": "", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead." },
    "connectionFactory": { "kind": "property", "displayName": "Connection Factory", "group": "common", "label": "", "required": false, "type": "object", "javaType": "javax.jms.ConnectionFactory", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "The connection factory to be use. A connection factory must be configured either on the component or endpoint." },
    "disableReplyTo": { "kind": "property", "displayName": "Disable Reply To", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "secret": false, "defaultValue": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Specifies whether Camel ignores the JMSReplyTo header in messages. If true, Camel does not send a reply back to the destination specified in the JMSReplyTo header. You can use this option if you want Camel to consume from a route and you do not want Camel to automatically send back a reply message because another component in your code handles the reply message. You can also use this option if you want to use Camel as a proxy between different message brokers and you want to route message from one system to another." },
    "durableSubscriptionName": { "kind": "property", "displayName": "Durable Subscription Name", "group": "common", "label": "", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "The durable subscriber name for specifying durable topic subscriptions. The clientId option must be configured as well." },
    "jmsMessageType": { "kind": "property", "displayName": "Jms Message Type", "group": "common", "label": "", "required": false, "type": "object", "javaType": "org.apache.camel.component.jms.JmsMessageType", "enum": [ "Bytes", "Map", "Object", "Stream", "Text" ], "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Allows you to force the use of a specific javax.jms.Message implementation for sending JMS messages. Possible values are: Bytes, Map, Object, Stream, Text. By default, Camel would determine which JMS message type to use from the In body type. This option allows you to specify it." }
  },
  "properties": {
    "httpUri": { "kind": "path", "displayName": "Http Uri", "group": "common", "label": "", "required": true, "type": "string", "javaType": "java.net.URI", "deprecated": false, "deprecationNote": "", "autowired": false, "secret": false, "description": "The URI to use such as http:\\/\\/hostname:port\\/path" },
    "bridgeEndpoint": { "kind": "parameter", "displayName": "Bridge Endpoint", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "autowired": false, "secret": false, "defaultValue": false, "description": "If the option is true, then the Exchange.HTTP_URI header is ignored, and use the endpoint's URI for request. You may also set the throwExceptionOnFailure to be false to let the AhcProducer send all the fault response back." },
    "bufferSize": { "kind": "parameter", "displayName": "Buffer Size", "group": "common", "label": "", "required": false, "type": "integer", "javaType": "int", "deprecated": false, "autowired": false, "secret": false, "defaultValue": 4096, "description": "The initial in-memory buffer size used when transferring data between Camel and AHC Client." },
    "headerFilterStrategy": { "kind": "parameter", "displayName": "Header Filter Strategy", "group": "common", "label": "", "required": false, "type": "object", "javaType": "org.apache.camel.spi.HeaderFilterStrategy", "deprecated": false, "autowired": false, "secret": false, "description": "To use a custom HeaderFilterStrategy to filter header to and from Camel message." },
    "throwExceptionOnFailure": { "kind": "parameter", "displayName": "Throw Exception On Failure", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "autowired": false, "secret": false, "defaultValue": true, "description": "Option to disable throwing the AhcOperationFailedException in case of failed responses from the remote server. This allows you to get all responses regardless of the HTTP status code." }
  },
  "apis": {
    "account": { "consumerOnly": false, "producerOnly": false, "description": "", "aliases": [ "^creator$=create", "^deleter$=delete", "^fetcher$=fetch", "^reader$=read", "^updater$=update" ], "methods": { "fetcher": { "description": "Create a AccountFetcher to execute fetch", "signatures": [ "com.twilio.rest.api.v2010.AccountFetcher fetcher()", "com.twilio.rest.api.v2010.AccountFetcher fetcher(String pathSid)" ] }, "updater": { "description": "Create a AccountUpdater to execute update", "signatures": [ "com.twilio.rest.api.v2010.AccountUpdater updater()", "com.twilio.rest.api.v2010.AccountUpdater updater(String pathSid)" ] } } },
    "address": { "consumerOnly": false, "producerOnly": false, "description": "", "aliases": [ "^creator$=create", "^deleter$=delete", "^fetcher$=fetch", "^reader$=read", "^updater$=update" ], "methods": { "creator": { "description": "Create a AddressCreator to execute create", "signatures": [ "com.twilio.rest.api.v2010.account.AddressCreator creator(String customerName, String street, String city, String region, String postalCode, String isoCountry)", "com.twilio.rest.api.v2010.account.AddressCreator creator(String pathAccountSid, String customerName, String street, String city, String region, String postalCode, String isoCountry)" ] }, "deleter": { "description": "Create a AddressDeleter to execute delete", "signatures": [ "com.twilio.rest.api.v2010.account.AddressDeleter deleter(String pathAccountSid, String pathSid)", "com.twilio.rest.api.v2010.account.AddressDeleter deleter(String pathSid)" ] }, "fetcher": { "description": "Create a AddressFetcher to execute fetch", "signatures": [ "com.twilio.rest.api.v2010.account.AddressFetcher fetcher(String pathAccountSid, String pathSid)", "com.twilio.rest.api.v2010.account.AddressFetcher fetcher(String pathSid)" ] }, "reader": { "description": "Create a AddressReader to execute read", "signatures": [ "com.twilio.rest.api.v2010.account.AddressReader reader()", "com.twilio.rest.api.v2010.account.AddressReader reader(String pathAccountSid)" ] }, "updater": { "description": "Create a AddressUpdater to execute update", "signatures": [ "com.twilio.rest.api.v2010.account.AddressUpdater updater(String pathAccountSid, String pathSid)", "com.twilio.rest.api.v2010.account.AddressUpdater updater(String pathSid)" ] } } },
  }
}
`,
      },
    ]).forEach(({ vfsType, config }) => {
      ;['', 'query='].forEach((query) => {
        ;['', 'cellformats='].forEach((cellformats) => {
          describe('jsonpathTableMacro tests', () => {
            ;['data1.json', 'data1-json5.json', 'data1.toml', 'data1.yml'].forEach((dataFile) => {
              it(`simple table test, ${type}, ${vfsType}, dataFile: ${dataFile}`, () => {
                const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

jsonpathTable::example$${dataFile}[${query}'$.rows[*]', ${cellformats}'name|description']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">foo</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">bar</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">barish</p></td>
</tr>
</tbody>
</table>`)
              })

              it(`templated table test, ${type}, ${vfsType}, dataFile: ${dataFile}`, () => {
                const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

jsonpathTable::example$${dataFile}[${query}'$.rows[*]', ${cellformats}'${delimiter}the \${name} described by${delimiter}|${delimiter}the description \${description}${delimiter}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the foo described by</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the bar described by</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description barish</p></td>
</tr>
</tbody>
</table>`)
              })

              it(`templated table test with escaped commas, ${type}, ${vfsType}, dataFile: ${dataFile}`, () => {
                const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

jsonpathTable::example$${dataFile}[${query}'$.rows[*]', ${cellformats}'${delimiter}the \${name} described by\\, incidentally\\,${delimiter}|${delimiter}the description \${description}${delimiter}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the foo described by, incidentally,</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the bar described by, incidentally,</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description barish</p></td>
</tr>
</tbody>
</table>`)
              })

              it(`object table test, ${type}, ${vfsType}, dataFile: ${dataFile}`, () => {
                const doc = f(`

[cols='1,2,3',options="header"]
|===
|name
|p1
|p2
|===

jsonpathTable::example$${dataFile}[${query}'nodes$.object.*', ${cellformats}'${delimiter}\${path[2]}${delimiter}|${delimiter}\${value.p1}${delimiter}|${delimiter}\${value.p2}${delimiter}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 16.6666%;">
<col style="width: 33.3333%;">
<col style="width: 50.0001%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">p1</th>
<th class="tableblock halign-left valign-top">p2</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field1</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">a</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">b</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field2</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">c</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">d</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field3</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">e</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">f</p></td>
</tr>
</tbody>
</table>`)
              })
            })
            it(`camel object table test, ${type}, ${vfsType}`, () => {
              const or = '\\|\\|'
              const doc = f(`
:expr: ${cellformats}'${delimiter}*\${path[2]}* (\${value.group})${delimiter}| \
${delimiter}\${value.description}${delimiter}| \
${delimiter}\${value.default ${or} ""}${delimiter}| \
${delimiter}\${value.type}${delimiter}'

[width="100%",cols="2,5,^1,2",options="header"]
|===
| Name | Description | Default | Type
|===

jsonpathTable::example$data2.json[${query}'nodes$.componentProperties.*', {expr}]

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 20%;">
<col style="width: 50%;">
<col style="width: 10%;">
<col style="width: 20%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">Name</th>
<th class="tableblock halign-left valign-top">Description</th>
<th class="tableblock halign-center valign-top">Default</th>
<th class="tableblock halign-left valign-top">Type</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>brokerURL</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">string</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>clientId</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">string</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>connectionFactory</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">The connection factory to be use. A connection factory must be configured either on the component or endpoint.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">object</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>disableReplyTo</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Specifies whether Camel ignores the JMSReplyTo header in messages. If true, Camel does not send a reply back to the destination specified in the JMSReplyTo header. You can use this option if you want Camel to consume from a route and you do not want Camel to automatically send back a reply message because another component in your code handles the reply message. You can also use this option if you want to use Camel as a proxy between different message brokers and you want to route message from one system to another.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">boolean</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>durableSubscriptionName</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">The durable subscriber name for specifying durable topic subscriptions. The clientId option must be configured as well.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">string</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>jmsMessageType</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Allows you to force the use of a specific javax.jms.Message implementation for sending JMS messages. Possible values are: Bytes, Map, Object, Stream, Text. By default, Camel would determine which JMS message type to use from the In body type. This option allows you to specify it.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">object</p></td>
</tr>
</tbody>
</table>`)
            })
          })
        })
        ;['', 'format='].forEach((format) => {
          describe(`jsonpathListMacro tests, ${type}, ${vfsType}`, () => {
            it('simple ulist array test', () => {
              const doc = f(`

jsonpathList::example$data1.json[${query}'$.rows[*]', ${format}'${delimiter}\${name}: \${description}${delimiter}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="ulist">
<ul>
<li>
<p>foo: fooish</p>
</li>
<li>
<p>bar: barish</p>
</li>
</ul>
</div>`)
            })

            it('simple ulist object test', () => {
              const doc = f(`

jsonpathList::example$data1.json[${query}'nodes$.object.*', ${format}'${delimiter}\${path[2]}: \${value.p1} and \${value.p2}${delimiter}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="ulist">
<ul>
<li>
<p>field1: a and b</p>
</li>
<li>
<p>field2: c and d</p>
</li>
<li>
<p>field3: e and f</p>
</li>
</ul>
</div>`)
            })

            it('simple olist array test', () => {
              const doc = f(`

jsonpathOrderedList::example$data1.json[${query}'$.rows[*]', ${format}'\`\${name}: \${description}\`']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="olist">
<ol class="">
<li>
<p>foo: fooish</p>
</li>
<li>
<p>bar: barish</p>
</li>
</ol>
</div>`)
            })

            it('simple olist object test', () => {
              const doc = f(`

jsonpathOrderedList::example$data1.json[${query}'nodes$.object.*', ${format}'\`\${path[2]}: \${value.p1} and \${value.p2}\`']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="olist">
<ol class="">
<li>
<p>field1: a and b</p>
</li>
<li>
<p>field2: c and d</p>
</li>
<li>
<p>field3: e and f</p>
</li>
</ol>
</div>`)
            })
          })
          describe(`jsonpathUniqueCountMacro tests, ${type}, ${vfsType}`, () => {
            it('simple unique count array test', () => {
              const doc = f(`

jsonpathUniqueCount:example$data1.json[${query}'$.rows[*\\]', ${format}'uniqueish']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
            })

            it('simple unique count object test', () => {
              const doc = f(`

jsonpathUniqueCount:example$data1.json[${query}'nodes$.object.*', ${format}"{backtick}\${value.uniqueish}{backtick}"]

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
            })
          })
        })
        ;['', 'subjectformat='].forEach((subjectformat) => {
          ;['', 'descriptionformat='].forEach((descriptionformat) => {
            describe('jsonpathDListMacro tests', () => {
              it('simple dlist array test', () => {
                const doc = f(`

jsonpathDescriptionList::example$data1.json[${query}'$.rows[*]', ${subjectformat}'\`\${name}\`', ${descriptionformat}'\`\${description}\`']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">foo</dt>
<dd>
<p>fooish</p>
</dd>
<dt class="hdlist1">bar</dt>
<dd>
<p>barish</p>
</dd>
</dl>
</div>`)
              })

              it('simple dlist object test', () => {
                const doc = f(`

jsonpathDescriptionList::example$data1.json[${query}'nodes$.object.*', ${subjectformat}'\`\${path[2]}\`', ${descriptionformat}'\`\${value.p1} and \${value.p2}\`']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">field1</dt>
<dd>
<p>a and b</p>
</dd>
<dt class="hdlist1">field2</dt>
<dd>
<p>c and d</p>
</dd>
<dt class="hdlist1">field3</dt>
<dd>
<p>e and f</p>
</dd>
</dl>
</div>`)
              })
            })
          })
        })
        describe('jsonpathCountMacro tests', () => {
          it('simple count array test', () => {
            const doc = f(`

jsonpathCount:example$data1.json[${query}'$.rows[*\\]']

`, config)
            const html = doc.convert()
            expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
          })

          it('simple count object test', () => {
            const doc = f(`

jsonpathCount:example$data1.json[${query}'nodes$.object.*']

`, config)
            const html = doc.convert()
            expect(html).to.equal(`<div class="paragraph">
<p>3</p>
</div>`)
          })
        })
        ;['', 'formats='].forEach((formats) => {
          ;['', 'target='].forEach((target) => {
            describe('jsonpathBlock tests', () => {
              it('simple block array test', () => {
                const doc = f(`

[jsonpathBlock, ${target}example$data1.json, ${query}'$.rows[*]', 'name,description,uniqueish']
----
== Section {name}

A description: {description} that is uniquely {uniqueish}
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect1">
<h2 id="_section_foo">Section foo</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A description: fooish that is uniquely 1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_bar">Section bar</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A description: barish that is uniquely 2</p>
</div>
</div>
</div>`)
              })
              it('simple block object test', () => {
                const doc = f(`

[jsonpathBlock, ${target}example$data1.json, ${query}'nodes$.object.*', formats='name=path[2],p1=value.p1,p2=value.p2']
----

== Section {name}

Attribute p1 has value {p1}, whereas attribute p2 has value {p2}.
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect1">
<h2 id="_section_field1">Section field1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value a, whereas attribute p2 has value b.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_field2">Section field2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value c, whereas attribute p2 has value d.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_field3">Section field3</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value e, whereas attribute p2 has value f.</p>
</div>
</div>
</div>`)
              })
              it('block ifeval test', () => {
                const doc = f(`
[jsonpathBlock,  ${target}example$data2.json, ${query}'nodes$.apis.*','name=path[2],consumeronly=value.consumerOnly,produceronly=value.producerOnly,neither=value.consumerOnly || value.producerOnly, methodcount=Object.keys(value.methods).length']
----

=== API: {name}

%ifeval::[{consumeronly} == true]
*Only consumer is supported*
%endif::[]
%ifeval::[{produceronly} == true]
*Only producer is supported*
%endif::[]
%ifeval::[{neither} == false]
*Both producer and consumer are supported*
%endif::[]

%ifeval::[{methodcount} == 0]
The {name} has no API methods.
%endif::[]
%ifeval::[{methodcount} != 0]
There are {methodcount} methods.
%endif::[]
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect2">
<h3 id="_api_account">API: account</h3>
<div class="paragraph">
<p><strong>Both producer and consumer are supported</strong></p>
</div>
<div class="paragraph">
<p>There are 2 methods.</p>
</div>
</div>
<div class="sect2">
<h3 id="_api_address">API: address</h3>
<div class="paragraph">
<p><strong>Both producer and consumer are supported</strong></p>
</div>
<div class="paragraph">
<p>There are 5 methods.</p>
</div>
</div>`)
              })
              it('block negative ifeval test', () => {
                const doc = f(`
[jsonpathBlock,  ${target}example$data2.json, ${query}'nodes$.apis[?(@.kind=="none")]','name=path[2],consumeronly=value.consumerOnly,produceronly=value.producerOnly,neither=value.consumerOnly || value.producerOnly, methodcount=Object.keys(value.methods).length']
----

=== API: {name}

%ifeval::[{consumeronly} == true]
*Only consumer is supported*
%endif::[]
%ifeval::[{produceronly} == true]
*Only producer is supported*
%endif::[]
%ifeval::[{neither} == false]
*Both producer and consumer are supported*
%endif::[]

%ifeval::[{methodcount} == 0]
The {name} has no API methods.
%endif::[]
%ifeval::[{methodcount} != 0]
There are {methodcount} methods.
%endif::[]
----

`, config)
                const html = doc.convert()
                expect(html).to.equal('')
              })
              it('block nested ifeval test', () => {
                const doc = f(`
[jsonpathBlock,  ${target}example$data2.json, ${query}'nodes$.apis.*','apiname=path[2], methodcount=Object.keys(value.methods).length']
----

=== API: {apiname}

%ifeval::[{methodcount} == 0]
The {apiname} has no API methods.
%endif::[]
%ifeval::[{methodcount} != 0]
There are {methodcount} methods.
%endif::[]

[jsonpathBlock, example$data2.json, 'nodes$.apis["{apiname}"].methods.*','methodname=path[4],signaturecount=Object.keys(value.signatures).length']
------
[#_api_{apiname}_method_{methodname}]
==== Method {methodname}

%%ifeval::[{signaturecount} == 0]
The {methodname} has no method signatures.
%%endif::[]
%%ifeval::[{signaturecount} != 0]
There are {signaturecount} method signatures.
%%endif::[]

------

----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect2">
<h3 id="_api_account">API: account</h3>
<div class="paragraph">
<p>There are 2 methods.</p>
</div>
<div class="sect3">
<h4 id="_api_account_method_fetcher">Method fetcher</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
<div class="sect3">
<h4 id="_api_account_method_updater">Method updater</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_api_address">API: address</h3>
<div class="paragraph">
<p>There are 5 methods.</p>
</div>
<div class="sect3">
<h4 id="_api_address_method_creator">Method creator</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
<div class="sect3">
<h4 id="_api_address_method_deleter">Method deleter</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
<div class="sect3">
<h4 id="_api_address_method_fetcher">Method fetcher</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
<div class="sect3">
<h4 id="_api_address_method_reader">Method reader</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
<div class="sect3">
<h4 id="_api_address_method_updater">Method updater</h4>
<div class="paragraph">
<p>There are 2 method signatures.</p>
</div>
</div>
</div>`)
              })
              it('block userRequire test', () => {
                const doc = f(`
[jsonpathBlock,  ${target}example$data2.json, ${query}'nodes$.apis.*','name=path[2],x=util(value.consumerOnly),y=util.ternary(value.consumerOnly\\,value.producerOnly), methodcount=Object.keys(value.methods).length','util=test/util/sample.js']
----

=== API: {name}

{x}

{y}

methodCount: {methodCount}
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect2">
<h3 id="_api_account">API: account</h3>
<div class="paragraph">
<p>argument is false</p>
</div>
<div class="paragraph">
<p>both x and y</p>
</div>
<div class="paragraph">
<p>methodCount: 2</p>
</div>
</div>
<div class="sect2">
<h3 id="_api_address">API: address</h3>
<div class="paragraph">
<p>argument is false</p>
</div>
<div class="paragraph">
<p>both x and y</p>
</div>
<div class="paragraph">
<p>methodCount: 5</p>
</div>
</div>`)
              })
            })
          })
        })
      })

      describe('jsonpathAttributes include processor tests', () => {
        it('object properties', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='$.component', formats='kind,name,mytitle=title,description']

kind: {kind}
name: {name}
title: {mytitle}
description: {description}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>kind: component
name: activemq
title: ActiveMQ
description: Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.</p>
</div>`)
        })

        it('simple name expressions', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='path[2]=value.description']

brokerURL: {brokerurl}
clientId: {clientid}
connectionFactory: {connectionfactory}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>brokerURL: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
clientId: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
connectionFactory: The connection factory to be use. A connection factory must be configured either on the component or endpoint.</p>
</div>`)
        })

        it('complex name expressions', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='\`\${path[2]}-description\`=value.description,\`\${path[2]}-javatype\`=value.javaType']

brokerURL description: {brokerurl-description}
brokerURL type: {brokerurl-javatype}
clientId description: {clientid-description}
clientId type: {clientid-javatype}
connectionFactory description: {connectionfactory-description}
connectionFactory type: {connectionfactory-javatype}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>brokerURL description: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
brokerURL type: java.lang.String
clientId description: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
clientId type: java.lang.String
connectionFactory description: The connection factory to be use. A connection factory must be configured either on the component or endpoint.
connectionFactory type: javax.jms.ConnectionFactory</p>
</div>`)
        })

        it('multiple uses', () => {
          const doc = f(`= My Page
include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='path[2]=value.description']
include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='\`\${path[2]}-description\`=value.description,\`\${path[2]}-javatype\`=value.javaType']
include::jsonpath$example$data2.json[query='$.component', formats='kind,name,mytitle=title,description']

:another-attribute3: foo

kind: {kind}
name: {name}
title: {mytitle}
description: {description}

brokerURL: {brokerurl}
clientId: {clientid}
connectionFactory: {connectionfactory}

brokerURL description: {brokerurl-description}
brokerURL type: {brokerurl-javatype}
clientId description: {clientid-description}
clientId type: {clientid-javatype}
connectionFactory description: {connectionfactory-description}
connectionFactory type: {connectionfactory-javatype}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>kind: component
name: activemq
title: ActiveMQ
description: Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.</p>
</div>
<div class="paragraph">
<p>brokerURL: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
clientId: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
connectionFactory: The connection factory to be use. A connection factory must be configured either on the component or endpoint.</p>
</div>
<div class="paragraph">
<p>brokerURL description: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
brokerURL type: java.lang.String
clientId description: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
clientId type: java.lang.String
connectionFactory description: The connection factory to be use. A connection factory must be configured either on the component or endpoint.
connectionFactory type: javax.jms.ConnectionFactory</p>
</div>`)
        })

        it('escaped special characters', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='$.component', formats='specialChars']

specialChars: {specialchars}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>specialChars: Here are some &lt;&amp;&gt; special &lt; characters &amp; with pointy &gt; brackets&gt;&gt;&lt;&lt;</p>
</div>`)
        })

        it('jsonpathcount include processor', () => {
          const doc = f(`include::jsonpathcount$example$data2.json[queries='pathparametercount=nodes$.properties[?(@.kind=="path")],queryparametercount=nodes$.properties[?(@.kind=="parameter")]']

pathparametercount: {pathparametercount}
queryparametercount: {queryparametercount}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>pathparametercount: 1
queryparametercount: 4</p>
</div>`)
        })
      })
    })
  })
})
