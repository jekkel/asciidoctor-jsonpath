= How to release to npm
:version: 0.1.3-rc.2
:repo: asciidoctor-jsonpath
:bundle: @djencks-asciidoctor-jsonpath
:url: https://gitlab.com/djencks/{repo}/-/jobs/artifacts/main/raw/{bundle}-{version}.tgz?job=bundle-stable


* Update this file, package.json, and the two README files with the new version.
* push to main at gitlab
* When the CI completes the bundle to release should be at:
{url}

* Run
npm publish --access public {url} --dry-run

* Run
npm publish --access public {url}
