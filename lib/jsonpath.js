'use strict'

const jp = require('jsonpath')
const json = require('json5')
const toml = require('@iarna/toml')
const yaml = require('yaml')
const path = require('path')
const reportSupport = require('@djencks/asciidoctor-report-support')

function yamlParse (yamlString) {
  try {
    return yaml.parse(yamlString)
  } catch (error) {
    if (error.code === 'MULTIPLE_DOCS') {
      return yaml
        .parseAllDocuments(yamlString)
        .map((doc) => { return doc.toJSON() })
    }
    throw error
  }
}

const parsers = {
  '.json': json.parse,
  '.toml': toml.parse,
  '.yaml': yamlParse,
  '.yml': yamlParse,
}

const writers = {
  json: (text) => JSON.stringify(text, null, 2),
  yml: yaml.stringify,
  text: (text) => text,
}

const JSONPATH_INCLUDE = 'jsonpath$'
const JSONPATH_COUNT_INCLUDE = 'jsonpathcount$'
const JSONPATH_UNIQUE_COUNT_INCLUDE = 'jsonpathuniquecount$'

let pipelineConfig = { playbookDir: undefined, debug: false, trace: false }

module.exports.register = function (registry, config) {
  const pc = reportSupport.pipelineConfigure(registry, config, module.exports, doQuery({}), '@djencks/asciidoctor-jsonpath', undefined, { json: asJson })
  if (pc) {
    //called as Antora extension register.
    pipelineConfig = pc
    return
  }

  //called as Asciidoctor extension register.

  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.

  //File access

  const vfs = (config.vfs && typeof config.vfs.read === 'function')
    ? config.vfs
    //Antora support
    : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
      ? {
        read: (resourceId) => {
          const target = config.contentCatalog.resolveResource(resourceId, config.file.src)
          if (target) return target.contents
          return undefined
        },
      }
      //look in the file system
      : fsAccess()

  function fsAccess () {
    const fs = require('fs')
    return {
      read: (path, encoding = 'utf8') => {
        if (path.startsWith('file://')) {
          return fs.readFileSync(path.substr('file://'.length), encoding)
        }
        return fs.readFileSync(path, encoding)
      },
    }
  }

  function writerFunction (outputFormat, logContext) {
    const writer = writers[outputFormat]
    if (!writer) {
      logContext.logger.warn({ msg: `unrecognized output format ${outputFormat}, must be 'text', 'json' or 'yml'`, ...logContext.use })
    }
    return writer
  }

  function doRegister (registry) {
    const reports = reportSupport('jsonpath', doQuery({}, vfs), pipelineConfig, registry, config)
    if (typeof registry.block === 'function') {
      registry.block(reports.templateBlockProcessor('jsonpathBlock', reportSupport.formatFunction, reports.processBlocks))
      registry.block(reports.templateBlockProcessor('jsonpathTemplate', reportSupport.formatFunction, reports.processTemplates))
    } else {
      reports.logger.warn('No \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(reports.tableProcessor('jsonpathTable', '|', reportSupport.formatFunction))
      registry.blockMacro(reports.descriptionListProcessor('jsonpathDescriptionList', reportSupport.formatFunction))
      registry.blockMacro(reports.listProcessor('jsonpathList', 'ulist', '*', reportSupport.formatFunction))
      registry.blockMacro(reports.listProcessor('jsonpathOrderedList', 'olist', '.', reportSupport.formatFunction))
      registry.blockMacro(reports.expressionProcessor('jsonpathExpression', reportSupport.formatFunction, reports.evaluateExpressionBlock,
        true, false, writerFunction))
    } else {
      reports.logger.warn('No \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(reports.inlineCountProcessor('jsonpathCount'))
      registry.inlineMacro(reports.inlineUniqueCountProcessor('jsonpathUniqueCount', reportSupport.formatFunction))
      registry.inlineMacro(reports.expressionProcessor('jsonpathExpression', reportSupport.formatFunction, reports.evaluateExpressionInline,
        true, false, writerFunction))
    } else {
      reports.logger.warn('No \'inlineMacro\' method on alleged registry')
    }
    if (typeof registry.includeProcessor === 'function') {
      registry.prefer('include_processor', reports.attributesIncludeProcessor(JSONPATH_INCLUDE, reportSupport.formatFunction, true))
      registry.prefer('include_processor', reports.countAttributesIncludeProcessor(JSONPATH_COUNT_INCLUDE))
      registry.prefer('include_processor', reports.uniqueCountAttributesIncludeProcessor(JSONPATH_UNIQUE_COUNT_INCLUDE, reportSupport.formatFunction, true))
    } else {
      reports.logger.warn('No \'includeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}

function jsonQuery (query, data, fn, logContext, transform = function (items) {
  return items
}) {
  const bits = query.split('$')
  const method = bits[0] || 'query'
  query = '$' + bits[1]
  transform = transform.bind({ data, jp })
  try {
    let result = jp[method](data, query)
    result = transform(result)
    result.forEach((item) => {
      '$' in item || (item.$ = data)
      fn(item)
    })
  } catch (e) {
    logContext.logger.warn({ msg: `Processing error for formatting or query ${query}, error: ${e.msg || e}`, ...logContext.use })
  }
  return query
}

function asJson (file, logContext) {
  return toJson(file.path, file.contents, logContext)
}

function toJson (target, contents, logContext) {
  const ext = path.extname(target)
  const parse = parsers[ext]
  if (parse) {
    try {
      return parse(contents.toString())
    } catch (error) {
      logContext.logger.warn({ msg: `Invalid source file ${target}`, error, ...logContext.use })
      // return
    }
  } else {
    logContext.logger.warn({ msg: `No parser for extension ${ext} for target {target}`, ...logContext.use })
    // return
  }
}

function doQuery (cache, vfs = null) {
  return (target, { query }, fn, contentCatalog, file, logContext, contentExtractor = null, transform) => {
    logContext.debug && logContext.logger.debug(Object.assign({ query, transform }, logContext.use))
    let data = cache[target]
    if (!data) {
      let found
      const contents = contentCatalog
        ? ((found = contentCatalog.resolveResource(target, file.src)) && found.contents)
        : vfs.read(target)
      if (contents) {
        data = toJson(target, contents, logContext)
        if (!data) {
          return
        }
        cache[target] = data
      } else {
        logContext.logger.warn({ msg: `Target ${target} not found`, ...logContext.use })
        return
      }
    }
    jsonQuery(query, data, fn, logContext, transform)
  }
}

module.exports.doQuery = doQuery
module.exports.jsonQuery = jsonQuery
